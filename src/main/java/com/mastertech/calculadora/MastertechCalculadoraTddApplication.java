package com.mastertech.calculadora;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MastertechCalculadoraTddApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechCalculadoraTddApplication.class, args);
	}

}
