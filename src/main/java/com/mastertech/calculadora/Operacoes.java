package com.mastertech.calculadora;

import java.util.List;

public class Operacoes {

    public static int soma(List<Integer> numeros) {
        int resultado = 0;
        for (Integer numero : numeros) {
            resultado += numero;
        }
        return resultado;
    }

    public static int soma(int num1, int num2) {
        return num1 + num2;
    }

    public static int subtracao(List<Integer> numeros) {
        int resultado = numeros.get(0);

        for (int i = 1; i < numeros.size(); i++) {
            resultado -= numeros.get(i);
        }
        return resultado;
    }

    public static int subtracao(int num1, int num2) {
        return num1 - num2;
    }

    public static int multiplicacao(List<Integer> numeros) {
        int resultado = 1;
        for (Integer numero : numeros) {
            resultado *= numero;
        }
        return resultado;
    }

    public static int multiplicacao(int num1, int num2) {
        return num1 * num2;
    }

    public static int divisao(int num1, int num2) {
        return num1 / num2;
    }
}
