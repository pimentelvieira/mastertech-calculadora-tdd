package com.mastertech.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class OperacoesTest {

    @Test
    public void testaSoma() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(2);
        numeros.add(5);
        numeros.add(7);
        Assertions.assertEquals(Operacoes.soma(numeros), 14);
    }

    @Test
    public void testaSomaDoisNumeros() {
        Assertions.assertEquals(Operacoes.soma(3, 2), 5);
    }

    @Test
    public void testaSubtracao() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(2);
        numeros.add(5);
        numeros.add(7);
        Assertions.assertEquals(Operacoes.subtracao(numeros), -10);
    }

    @Test
    public void testaSubtracaoDoisNumeros() {
        Assertions.assertEquals(Operacoes.subtracao(3, 2), 1);
    }

    @Test
    public void testaMultiplicacao() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(2);
        numeros.add(5);
        numeros.add(7);
        Assertions.assertEquals(Operacoes.multiplicacao(numeros), 70);
    }

    @Test
    public void testaMultiplicacaoDoisNumeros() {
        Assertions.assertEquals(Operacoes.multiplicacao(3, 2), 6);
    }

    @Test
    public void testaDivisao() {
        Assertions.assertEquals(Operacoes.divisao(20, 5), 4);
    }

    @Test
    public void testaDivisaoPorZero() {
        Assertions.assertThrows(ArithmeticException.class, () -> Operacoes.divisao(20, 0));
    }
}
